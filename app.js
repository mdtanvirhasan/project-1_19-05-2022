
let weather={ apikey:"e0e2942fbdeeddf5435301a0f0a44081",



 fetchWeather: function(city){
  fetch("https://api.openweathermap.org/data/2.5/weather?q="+city+"&units=metric&appid="+this.apikey).then(res=>res.json()).then(data=>{
         this.displayWeather(data);
     })
     },

     fetchLocalWeather: function(lat,lon){
      fetch("https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&units=metric&appid="+this.apikey).then(res=>res.json()).then(data=>{
        //console.log(data);     
      this.displayWeather(data);
         })
         },

     fetchDetailedWeather:function(city){
       fetch("https://api.openweathermap.org/data/2.5/forecast?q="+city+"&units=metric&appid="+this.apikey).then(res=>res.json()).then(data=>{
        this.displayForecast(data);
    })
     },

displayWeather: function(data){
  const {name}=data;
  const {icon,description}=data.weather[0];
  const {temp_min,humidity,}=data.main;
  const {temp_max}=data.main;
  const {feels_like,pressure}=data.main;
  const {wind_speed}=data.wind;

  console.log(name,icon,description,temp_max,temp_min,humidity);
  document.querySelector("div.container h2[name='placeName']").innerText="Weather in "+name;
  document.querySelector(".icon").src="http://openweathermap.org/img/wn/"+icon+".png";
  document.querySelector(".description").innerText = description.toUpperCase();
  document.querySelector(".hi-temp").innerText = "Highest Temperature: "+temp_max.toFixed(2)+"°C | "+this.c2f(temp_max).toFixed(2)+"°F";
  document.querySelector(".low-temp").innerText = "Lowest Temperature: "+temp_min.toFixed(2)+"°C | "+this.c2f(temp_min).toFixed(2)+"°F";
  document.querySelector(" .humidity").innerText="Humidity: "+humidity+"%";
  document.querySelector(" .feelsLike").innerText="Feels Like: "+feels_like.toFixed(2)+"°C | "+this.c2f(feels_like).toFixed(2)+"°F";
  document.querySelector(" .pressure").innerText="Pressure: "+pressure;
  //document.querySelector(" .wind_speed").innerText="Wind Speed: "+wind_speed;
  document.getElementById("container").style.visibility='visible';

  this.fetchDetailedWeather(name);
},

displayForecast: function(data){
  

  for(var i=0;i<8;i++){
    const {dt_txt}=data.list[i];
    const {icon,description}=data.list[i].weather[0];
    const {temp_min,temp_max}=data.list[i].main;

    console.log(dt_txt,icon,description,temp_max,temp_min);
    document.getElementById("slide-"+(i+1)).innerText=dt_txt;
    document.getElementById("img"+(i+1)).src="http://openweathermap.org/img/wn/"+icon+".png";
    document.getElementById("slide-"+(i+1)+"Min").innerText="Hi: "+temp_min.toFixed(2)+"°C | "+this.c2f(temp_min).toFixed(2)+"°F";
    document.getElementById("slide-"+(i+1)+"Max").innerText="Low: "+temp_max.toFixed(2)+"°C | "+this.c2f(temp_max).toFixed(2)+"°F";


  }

}

,search: function(){
  this.fetchWeather(document.querySelector(".location").value)
  this.fetchDetailedWeather(document.querySelector(".location").value)
  console.log(document.querySelector(".location").value)

},c2f: function(c){
  return c* 9 / 5 + 32;
},

geoUpdate:function(){
  this.showLocation(navigator.geolocation.getCurrentPosition(showLocation))

  }

};

function showLocation(position)  {
  // console.log(position);
  const latitude=position.coords.latitude;
  const longitude=position.coords.longitude;
  console.log(latitude,longitude);
  weather.fetchLocalWeather(latitude,longitude);
  


}
document.querySelector(".submit").addEventListener("click",function(e){
  e.preventDefault();
  

  weather.search();
  document.getElementById("location").value="";
})

document.querySelector(".localUpdates").addEventListener("click",function(e){
  weather.geoUpdate();
})

weather.geoUpdate();







